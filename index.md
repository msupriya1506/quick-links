# AWS Quick Links

## [AWS Services - Analytics](#aws-services---analytics)

- [Amazon Athena](https://console.aws.amazon.com/athena/home)
- [Amazon EMR](https://console.aws.amazon.com/elasticmapreduce/home/?pg=ln&cp=bn)
- [Amazon Kinesis Data Firehose](https://console.aws.amazon.com/kinesis/home?p=pm&c=kinesis&z=1)
- [Amazon Kinesis Data Streams](https://console.aws.amazon.com/kinesis/home?p=pm&c=kinesis&z=1)
- [Amazon OpenSearch Service](https://console.aws.amazon.com/aos/home?refid=0c835ec3-3fbf-4e72-802c-19a02d6e2337)
- [Amazon QuickSight](https://portal.aws.amazon.com/billing/signup?client=quicksight&fid=441BE2A63D1F1F56-313F2AF2462BDF3C&redirect_url=https%3A%2F%2Fquicksight.aws.amazon.com%2Fsn%2Fconsole%2Fsignup#/start)

## [AWS Services - Application Integration](#aws-services---application-integration)

- [Amazon AppFlow](https://console.aws.amazon.com/appflow/home)
- [Amazon EventBridge (Amazon CloudWatch Events)](https://portal.aws.amazon.com/gp/aws/developer/registration/index.html?type=resubscribe&p=evtb&cp=bn&ad=a&refid=0c835ec3-3fbf-4e72-802c-19a02d6e2337)

## [AWS Services - Compute](#aws-services---compute)

- [AWS App Runner](https://us-east-1.console.aws.amazon.com/apprunner/home?region=us-east-1#/welcome)
- [Amazon EC2](https://console.aws.amazon.com/ec2/v2/home?p=pm&c=ec2&z=1)
- [Amazon EC2 Auto Scaling](https://aws.amazon.com/ec2/autoscaling/getting-started/)
- [EC2 Image Builder](https://us-east-1.console.aws.amazon.com/imagebuilder?refid=2738afd4-9401-4d18-8e3e-1b1c194dea07&region=us-east-1#/landingPage)
- [AWS Elastic Beanstalk](https://us-east-1.console.aws.amazon.com/elasticbeanstalk/home?region=us-east-1#/welcome)
- [AWS Serverless Application Repository](https://serverlessrepo.aws.amazon.com/applications)


## [AWS Services - Containers](#aws-services---containers)

- [AWS App2Container](https://aws.amazon.com/app2container/)
- [AWS Copilot](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/getting-started-aws-copilot-cli.html)
- [Amazon Elastic Container Registry (Amazon ECR)](https://console.aws.amazon.com/ecr/home/?refid=2738afd4-9401-4d18-8e3e-1b1c194dea07)
- [Amazon Elastic Container Service (Amazon ECS)](https://aws.amazon.com/ecs/getting-started/?pg=ln&cp=bn)
- [Amazon Elastic Kubernetes Service (Amazon EKS)](https://us-east-1.console.aws.amazon.com/eks/home?region=us-east-1#)
- [Amazon EKS Distro](https://console.aws.amazon.com/eks/home?p=lgt&cp=bn&ad=c)
- [AWS Fargate](https://console.aws.amazon.com/ecs#)
- [Red Hat OpenShift Service on AWS (ROSA)](https://www.redhat.com/en/technologies/cloud-computing/openshift/aws)


## [AWS Services - Database](#aws-services---database)

- [Amazon Aurora](https://console.aws.amazon.com/rds/home)
- [Amazon Aurora Serverless v2](https://console.aws.amazon.com/rds/home?p=ara&cp=bn&ad=c&refid=fb8718a7-d9f7-4e07-9fc5-b85de26b4178)
- [AWS Database Migration Service (AWS DMS)](https://signin.aws.amazon.com/signin?redirect_uri=https%3A%2F%2Fconsole.aws.amazon.com%2Fconsole%2Fhome%3Fad%3Dc%26cp%3Dbn%26hashArgs%3D%2523%26isauthcode%3Dtrue%26p%3Ddms%26state%3DhashArgsFromTB_us-west-2_0200ca96fd0f2b54&client_id=arn%3Aaws%3Asignin%3A%3A%3Aconsole%2Fcanvas&forceMobileApp=0&code_challenge=jlAA9YhlhtHxgGoUQBs_Xd-j-eFdErlHRlfirhbO-aM&code_challenge_method=SHA-256)
- [Amazon DocumentDB (with MongoDB compatibility)](https://docs.aws.amazon.com/documentdb/latest/developerguide/what-is.html)
- [Amazon DynamoDB](https://signin.aws.amazon.com/signin?redirect_uri=https%3A%2F%2Fus-east-2.console.aws.amazon.com%2Fdynamodb%3Fad%3Dc%26cp%3Dbn%26hashArgs%3D%2523%26isauthcode%3Dtrue%26p%3Dddb%26refid%3D0adc398c-f5a3-48bb-9d92-92215986ba48%257Eha_awssm-4107_aware%26region%3Dus-east-2%26state%3DhashArgsFromTB_us-east-2_04310dcbd7462c08&client_id=arn%3Aaws%3Asignin%3A%3A%3Aconsole%2Fdynamodb&forceMobileApp=0&code_challenge=j1uIKZ7sc4Pzx3wVlCYgk6pzMW5VJ2yqKnqM0Adfuns&code_challenge_method=SHA-256)
- [Amazon ElastiCache](https://console.aws.amazon.com/elasticache/home)
- [Amazon MemoryDB for Redis](https://console.aws.amazon.com/memorydb/home?refid=fb8718a7-d9f7-4e07-9fc5-b85de26b4178)
- [Amazon RDS](https://signin.aws.amazon.com/signin?redirect_uri=https%3A%2F%2Fus-east-2.console.aws.amazon.com%2Frds%2F%3Frefid%3D0adc398c-f5a3-48bb-9d92-92215986ba48%7Eha_awssm-4107_aware%26region%3Dus-east-2%26state%3DhashArgs%2523%26isauthcode%3Dtrue&client_id=arn%3Aaws%3Aiam%3A%3A015428540659%3Auser%2Frds&forceMobileApp=0&code_challenge=TOSSmTLBS2MAWYLA7iyNKSPdMGDAjO7SGrZlQzPOjWM&code_challenge_method=SHA-256)
- [Amazon Redshift](https://signin.aws.amazon.com/signin?redirect_uri=https%3A%2F%2Fus-east-1.console.aws.amazon.com%2Fredshiftv2%2Fhome%3Frefid%3D8e5f044b-6475-484a-b7eb-8923cfdd5362%26region%3Dus-east-1%26skipRegion%3Dtrue%26state%3DhashArgs%2523%26isauthcode%3Dtrue&client_id=arn%3Aaws%3Aiam%3A%3A015428540659%3Auser%2Fredshift&forceMobileApp=0&code_challenge=5tpu-0nAc70X1qYanc7YQRq0z1bHJmqmGze0zbnwuRw&code_challenge_method=SHA-256)



## [AWS Services - Storage](#aws-services---storage)

- [AWS Backup](https://console.aws.amazon.com/backup)
- [Amazon Elastic Block Store (Amazon EBS)](https://aws.amazon.com/ebs/)
- [AWS Elastic Disaster Recovery (CloudEndure Disaster Recovery)](https://console.aws.amazon.com/drs/home?refid=a049d6a3-611f-4ee9-9d3b-e88a4b66272f)
- [Amazon Elastic File System (Amazon EFS)](https://console.aws.amazon.com/efs)
- [Amazon FSx for Lustre](https://console.aws.amazon.com/fsx)
- [Amazon FSx for NetApp ONTAP](https://signin.aws.amazon.com/signin?redirect_uri=https%3A%2F%2Fconsole.aws.amazon.com%2Ffsx%2Fhome%3Fstate%3DhashArgs%2523%26isauthcode%3Dtrue&client_id=arn%3Aaws%3Aiam%3A%3A015428540659%3Auser%2Ffsx&forceMobileApp=0&code_challenge=C6tOz5MTslY0sid0S8B1BBOlUZgbskaabF4_nrxZyGo&code_challenge_method=SHA-256)
- [Amazon FSx for OpenZFS](https://aws.amazon.com/fsx/openzfs/getting-started/)
- [Amazon FSx for Windows File Server](https://console.aws.amazon.com/fsx)
- [Amazon S3](https://s3.console.aws.amazon.com/s3/home?p=s3&cp=bn&ad=c)
- [Amazon S3 Glacier](https://s3.console.aws.amazon.com/s3/home)
- [AWS Storage Gateway](https://console.aws.amazon.com/storagegateway/)


## [AWS Services - Developer Tools](#aws-services---developer-tools)

- [AWS Cloud Development Kit (AWS CDK)](https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html)
- [AWS CloudShell](https://us-east-1.console.aws.amazon.com/cloudshell/home?region=us-east-1#25029293-b7b7-408a-a8c3-7c06cd5bb8c1)
- [AWS CodeArtifact](https://console.aws.amazon.com/codesuite/codeartifact/start)
- [AWS CodeBuild](https://console.aws.amazon.com/codesuite/codebuild/home)
- [AWS CodeCommit](https://us-east-1.console.aws.amazon.com/codesuite/codecommit/repositories?region=us-east-1)
- [AWS CodeDeploy](https://us-east-1.console.aws.amazon.com/codesuite/codedeploy/deployments?region=us-east-1&deployments-meta=eyJmIjp7InRleHQiOiIifSwicyI6e30sIm4iOjUwLCJpIjowfQ)
- [Amazon CodeGuru](https://us-east-1.console.aws.amazon.com/codeguru/home?region=us-east-1#)
- [AWS CodePipeline](https://us-east-1.console.aws.amazon.com/codesuite/codepipeline/pipelines?region=us-east-1&pipelines-meta=eyJmIjp7InRleHQiOiIifSwicyI6eyJwcm9wZXJ0eSI6InVwZGF0ZWQiLCJkaXJlY3Rpb24iOi0xfSwibiI6MzAsImkiOjB9)
- [AWS CodeStar](https://us-east-1.console.aws.amazon.com/codesuite/codestar/home?region=us-east-1)
- [AWS Command Line Interface (AWS CLI)](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)
- [AWS Fault Injection Simulator](https://aws.amazon.com/fis/)
- [AWS SDKs and Tools](https://aws.amazon.com/developer/tools/)
- [AWS X-Ray](https://console.aws.amazon.com/xray/home)


## [AWS Services - Management and Governance](#aws-services---management-and-governance)

- [AWS Auto Scaling](https://console.aws.amazon.com/awsautoscaling/)
- [AWS CloudFormation](https://console.aws.amazon.com/cloudformation/?pg=ln&cp=bn&refid=b8b87cd7-09b8-4229-a529-91943319b8f5)
- [AWS CloudTrail](https://aws.amazon.com/cloudtrail/)
- [Amazon CloudWatch](https://console.aws.amazon.com/cloudwatch/home)
- [Amazon CloudWatch Logs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/CWL_GettingStarted.html)
- [AWS Compute Optimizer](https://console.aws.amazon.com/compute-optimizer/home)
- [AWS Config](https://aws.amazon.com/config/)
- [AWS Control Tower](https://console.aws.amazon.com/controltower/home)
- [AWS Health](https://docs.aws.amazon.com/health/latest/ug/health-api.html)
- [AWS License Manager](https://us-east-1.console.aws.amazon.com/license-manager/home?region=us-east-1#/gettingStarted)
- [Amazon Managed Grafana](https://console.aws.amazon.com/grafana/)
- [Amazon Managed Service for Prometheus](https://console.aws.amazon.com/prometheus/home)
- [AWS OpsWorks](https://aws.amazon.com/opsworks/)
- [AWS Organizations](https://aws.amazon.com/organizations/)
- [AWS Personal Health Dashboard](https://health.aws.amazon.com/health/home#/account/dashboard/open-issues)
- [AWS Proton](https://console.aws.amazon.com/proton/home?refid=b8b87cd7-09b8-4229-a529-91943319b8f5)
- [AWS Resilience Hub](https://console.aws.amazon.com/resiliencehub/home)
- [AWS Service Catalog](https://console.aws.amazon.com/servicecatalog)
- [AWS Systems Manager](https://console.aws.amazon.com/systems-manager/?refid=b8b87cd7-09b8-4229-a529-91943319b8f5)
- [AWS Trusted Advisor](https://us-east-1.console.aws.amazon.com/trustedadvisor#/landing)

## [AWS Services - Networking and Content Delivery](#aws-services---networking-and-content-delivery)

- [Amazon API Gateway](https://aws.amazon.com/api-gateway/)
- [AWS Client VPN](https://aws.amazon.com/vpn/client-vpn/)
- [Amazon CloudFront](https://aws.amazon.com/cloudfront/)
- [Elastic Load Balancing (ELB)](https://aws.amazon.com/elasticloadbalancing/getting-started/?nc=sn&loc=4&refid=b8b87cd7-09b8-4229-a529-91943319b8f5)
- [AWS PrivateLink](https://portal.aws.amazon.com/gp/aws/developer/registration/index.html?type=resubscribe&p=plk&cp=bn&ad=a)
- [Amazon Route 53](https://console.aws.amazon.com/route53/)
- [AWS Site-to-Site VPN](https://us-west-2.console.aws.amazon.com/vpc/home?#VpnConnections:sort=VpnConnectionId)
- [AWS Transit Gateway](https://console.aws.amazon.com/vpc/home?#TransitGateways:sort=transitGatewayId)
- [Amazon VPC](https://aws.amazon.com/vpc/)


## [AWS Services - Security, Identity, and Compliance](#aws-services---security-identity-and-compliance)

- [AWS Certificate Manager (ACM)](https://us-east-1.console.aws.amazon.com/acm/home?region=us-east-1#/welcome)
- [AWS CloudHSM](https://console.aws.amazon.com/cloudhsm/)
- [Amazon Cognito](https://console.aws.amazon.com/cognito/home?p=pm&c=cog&z=1)
- [Amazon Detective](https://signin.aws.amazon.com/signin?redirect_uri=https%3A%2F%2Fus-east-1.console.aws.amazon.com%2Fdetective%2Fhome%3Fregion%3Dus-east-1%26skipRegion%3Dtrue%26state%3DhashArgs%2523%26isauthcode%3Dtrue&client_id=arn%3Aaws%3Aiam%3A%3A015428540659%3Auser%2Fmorocco&forceMobileApp=0&code_challenge=GCRRfjMPvU1fN2TcQ1TX0pr-s_jLmkROXPpMszYlu3E&code_challenge_method=SHA-256)
- [AWS Directory Service](https://console.aws.amazon.com/directoryservicev2/identity/home)
- [Amazon GuardDuty](https://aws.amazon.com/guardduty/)
- [AWS Identity and Access Management (IAM)](https://us-east-1.console.aws.amazon.com/iamv2/home?)
- [Amazon Inspector](https://console.aws.amazon.com/inspector/v2/home)
- [AWS Key Management Service (AWS KMS)](https://signin.aws.amazon.com/signin?redirect_uri=https%3A%2F%2Fportal.aws.amazon.com%2Fbilling%2Fsignup%2Fresume&client_id=signup&code_challenge_method=SHA-256&code_challenge=B-vt1y7edZdpk3zlo2mrgg6zo0LCW9cF-lk2QkQkHyw)
- [Amazon Macie](https://signin.aws.amazon.com/signin?redirect_uri=https%3A%2F%2Faws.amazon.com%2Fmarketplace%2Fmanagement%2Fsignin%3Fstate%3DhashArgs%2523%26isauthcode%3Dtrue&client_id=arn%3Aaws%3Aiam%3A%3A015428540659%3Auser%2Faws-mp-seller-management-portal&forceMobileApp=0&code_challenge=c_SeaxBQFNoG89XlVtgqKHiz27DbTYkIBJDP7t9FdJw&code_challenge_method=SHA-256)
- [AWS Network Firewall](https://console.aws.amazon.com/vpc/home#NetworkFirewalls)
- [AWS Resource Access Manager (AWS RAM)](https://docs.aws.amazon.com/ram/latest/userguide/what-is.html)
- [AWS Secrets Manager](https://us-east-1.console.aws.amazon.com/secretsmanager/landing?region=us-east-1)
- [AWS Security Hub](https://console.aws.amazon.com/securityhub/home/?pg=ln&cp=bn)
- [AWS Security Token Service (AWS STS)](https://wa.aws.amazon.com/wat.concept.sts.en.html)
- [AWS Shield](https://console.aws.amazon.com/wafv2/shieldv2)
- [AWS Single Sign-On](https://aws.amazon.com/what-is/sso/)
- [AWS WAF](https://console.aws.amazon.com/wafv2/homev2/?&state=hashArgs%23)


## [AWS Services - Serverless](#aws-services---serverless)

- [AWS Lambda](https://us-east-1.console.aws.amazon.com/lambda/home?region=us-east-1#/begin)
- [AWS Serverless Application Model (AWS SAM)](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html)
- [Amazon Simple Email Service (SES)](https://aws.amazon.com/ses/)
- [Amazon Simple Notification Service (Amazon SNS)](https://console.aws.amazon.com/sns)
- [Amazon Simple Queue Service (Amazon SQS)](https://console.aws.amazon.com/sqs/home)
- [AWS Step Functions](https://console.aws.amazon.com/states/home)

